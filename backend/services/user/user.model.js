const mongoose = require('mongoose');

const User = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    fullname: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});
User.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = mongoose.model('User', User);