const User = require('./user.model');

const perform = (operation) => async (req, res) => {
    try {
        return res.json(await operation(req));
    } catch(e) {
        return res.status(400).json(e);
    }
};

module.exports = {
    getAll: perform(()  => User.find({})),
    getOne: perform(req => User.findById(req.params.id)),
    create: perform(req => new User(req.body).save()),
    update: perform(req => User.findByIdAndUpdate(req.params.id, req.body, { new: true })),
    delete: perform(req => User.findByIdAndRemove(req.params.id))
};
