const express = require('express');

const cUser = require('./services/user/user.controller');

const rUser = express.Router();

rUser.get('/',          cUser.getAll);
rUser.get('/:id',       cUser.getOne);
rUser.post('/',         cUser.create);
rUser.post('/:id',      cUser.update);
rUser.delete('/:id',    cUser.delete);

module.exports = {
    user: rUser
};
