const express = require('express');
const config = require('config');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const router = require('./router');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.use('/api/users', router.user);

const port = config.get('port');

(async function() {
    try {
        await mongoose.connect(config.get('db').url);
    }
    catch(e) {
        console.error(e.message);
        process.exit(999);
    }

    app.listen(port, (error) => {
        if (error) {
            console.error(error);
        } else {
            console.info('==> 🌎 Listening on port %s.', port);
        }
    });
})();
