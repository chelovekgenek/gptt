import { Router, Route, Redirect, IndexRedirect } from 'react-router'

import * as wrappers from 'components/wrappers'
import { App } from 'components/routes'

import * as r from 'constants/routes'

const renderRoutes = ({ history }) => (
    <Router history={history}>
        <Route path={r.INDEX} component={wrappers.App} >
            <IndexRedirect to='/app' />
            <Route path='/app' component={App} />
            <Redirect from={r.ANY} to={r.INDEX} />
        </Route>
    </Router>
)

renderRoutes.propTypes = {
    history: PropTypes.object.isRequired
}

export default renderRoutes
