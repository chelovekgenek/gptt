import { Button, Modal, Icon, Header, Form, Message } from 'semantic-ui-react'

const isUsernameValid = val => val.length !== 0
const isEmailValid = val => val.length !== 0

export default class UserModal extends React.PureComponent {
    static propTypes = {
        onInvert: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        clear: PropTypes.func.isRequired,
        mode: PropTypes.oneOf(['create', 'edit']).isRequired,
        data: PropTypes.shape({
            username: PropTypes.string.isRequired,
            email: PropTypes.string.isRequired,
            fullname: PropTypes.string
        }),
        error: PropTypes.shape
    }
    static defaultProps = {
        data: {
            username: '',
            email: '',
            fullname: ''
        },
        error: {
            name: '',
            fields: []
        }
    }
    constructor(props) {
        super(props)

        const data = props.data
        if (props.mode === 'edit') {
            data.username = props.data.username
            data.email = props.data.email
            data.fullname = props.data.fullname
        }
        this.state = {
            data,
            errors: {
                username: false,
                email: false
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        const fields = nextProps.error.fields
        if (fields.length !== 0) {
            this.setState({
                errors: {
                    ...this.state.errors,
                    ...fields.reduce((acc, cur) => {
                        acc[cur] = true
                        return acc
                    }, {})
                }
            })
        }
    }
    componentWillUnmount() {
        this.props.clear()
    }
    render() {
        const { mode } = this.props
        return (
            <Modal
                open
                closeIcon
                onClose={this.props.onInvert}
            >
                { mode === 'create' && (<Header icon='archive' content='Create new User' />)}
                { mode === 'edit' && (<Header icon='archive' content='Edit existing User' />)}
                <Modal.Content>
                    <Form error={this.props.error.fields.length !== 0}>
                        <Form.Input
                            fluid
                            required
                            error={this.state.errors.username}
                            label='Username'
                            value={this.state.data.username}
                            onChange={this.handleInput('username', isUsernameValid)}
                        />
                        <Form.Input
                            fluid
                            required
                            error={this.state.errors.email}
                            label='E-mail'
                            value={this.state.data.email}
                            onChange={this.handleInput('email', isEmailValid)}
                        />
                        <Form.Input
                            fluid
                            label='Full name'
                            value={this.state.data.fullname}
                            onChange={this.handleInput('fullname')}
                        />
                        <Message
                            error
                            header={this.props.error.name}
                        />
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button color='green' onClick={this.onSubmit}>
                        <Icon name='checkmark' />
                        { mode === 'create' && (<span>Create</span>)}
                        { mode === 'edit' && (<span>Update</span>)}
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
    handleInput = (field, validator = () => true) => (e) => {
        const value = e.target.value
        this.setState({
            data: { ...this.state.data, [field]: value },
            errors: { ...this.state.errors, [field]: !validator(value) }
        })
    }
    onSubmit = () => {
        const { data, errors } = this.state
        const newErrors = {}
        if (isUsernameValid(data.username) === false) {
            newErrors.username = true
        }
        if (isEmailValid(data.email) === false) {
            newErrors.email = true
        }
        if (Object.keys(newErrors).length !== 0) {
            return this.setState({ errors: { ...errors, ...newErrors } })
        }
        return this.props.onSubmit(data)
    }
}
