import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Table, Button, Loader /* , Menu, Icon */ } from 'semantic-ui-react'

import * as a from 'actions/users'

import UserModal from './user-modal'
import styles from './app.module.scss'

class App extends React.PureComponent {
    static propTypes = {
        get: PropTypes.func.isRequired,
        create: PropTypes.func.isRequired,
        updateById: PropTypes.func.isRequired,
        deleteById: PropTypes.func.isRequired,
        clearErrors: PropTypes.func.isRequired,
        app: PropTypes.shape({
            fetching: PropTypes.bool.isRequired,
            data: PropTypes.array.isRequired,
            selected: PropTypes.shape({
                id: PropTypes.string,
                fetching: PropTypes.bool.isRequired
            }).isRequired,
            error: PropTypes.shape({
                name: PropTypes.string,
                fields: PropTypes.array
            }).isRequired
        }).isRequired
    }
    state = {
        isCreateModalOpen: false,
        isEditModalOpen: false,
        selected: ''
    }
    componentDidMount() {
        this.props.get()
    }
    render() {
        const { fetching, data, selected } = this.props.app
        return (
            <div className={styles.container}>
                { fetching === true
                    ? (<Loader size='huge' active>Loading</Loader>)
                    : (
                        <div>
                            <div className={styles['action-bar']}>
                                <Button
                                    onClick={this.getModalHandler('isCreateModalOpen')}
                                    className={styles.add}
                                    icon='plus'
                                    color='green'
                                />
                            </div>
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell>Username</Table.HeaderCell>
                                        <Table.HeaderCell>Email</Table.HeaderCell>
                                        <Table.HeaderCell>Full name</Table.HeaderCell>
                                        <Table.HeaderCell />
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    { data.map(({ _id, ...item }) => (
                                        <Table.Row key={_id}>
                                            <Table.Cell>{ item.username }</Table.Cell>
                                            <Table.Cell>{ item.email }</Table.Cell>
                                            <Table.Cell>{ item.fullname }</Table.Cell>
                                            <Table.Cell collapsing>
                                                <Button
                                                    onClick={this.getModalHandler('isEditModalOpen', { selected: _id })}
                                                    icon='pencil'
                                                />
                                                <Button
                                                    onClick={() => this.props.deleteById(_id)}
                                                    icon='trash'
                                                    loading={selected.id === _id && selected.fetching === true}
                                                />
                                            </Table.Cell>
                                        </Table.Row>
                                    )) }
                                </Table.Body>

                                {/* <Table.Footer>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='3'>
                                            <Menu floated='right' pagination>
                                                <Menu.Item as='a' icon>
                                                    <Icon name='left chevron' />
                                                </Menu.Item>
                                                <Menu.Item as='a'>1</Menu.Item>
                                                <Menu.Item as='a'>2</Menu.Item>
                                                <Menu.Item as='a'>3</Menu.Item>
                                                <Menu.Item as='a'>4</Menu.Item>
                                                <Menu.Item as='a' icon>
                                                    <Icon name='right chevron' />
                                                </Menu.Item>
                                            </Menu>
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Footer> */}
                            </Table>
                            { this.state.isCreateModalOpen && (
                                <UserModal
                                    mode='create'
                                    onInvert={this.getModalHandler('isCreateModalOpen')}
                                    onSubmit={this.props.create}
                                    clear={this.props.clearErrors}
                                    error={this.props.app.error}
                                />
                            ) }
                            { this.state.isEditModalOpen && (
                                <UserModal
                                    mode='edit'
                                    data={data.find(item => item._id === this.state.selected)}
                                    onInvert={this.getModalHandler('isEditModalOpen')}
                                    onSubmit={d => this.props.updateById(this.state.selected, d)}
                                    clear={this.props.clearErrors}
                                    error={this.props.app.error}
                                />
                            ) }
                        </div>
                    )
                }
            </div>
        )
    }
    getModalHandler = (field, args = {}) => callback => this.setState(
        s => ({ [field]: !s[field], ...args }),
        typeof callback === 'function' ? callback : () => {}
    )
}

export default connect(
    state => ({
        app: state.rootReducer.app
    }),
    dispatch => ({
        clearErrors: bindActionCreators(a.clearErrors, dispatch),
        get: bindActionCreators(a.getRequest, dispatch),
        create: bindActionCreators(a.createRequest, dispatch),
        updateById: bindActionCreators(a.updateByIdRequest, dispatch),
        deleteById: bindActionCreators(a.deleteByIdRequest, dispatch)
    })
)(App)
