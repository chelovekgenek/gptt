const App = ({ children }) => (
    <div>{ children }</div>
)

App.propTypes = {
    children: PropTypes.node.isRequired
}

export default App
