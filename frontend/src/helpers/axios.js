import axios from 'axios'

const instance = axios.create({
    baseURL: Config.backend.url,
    timeout: 3000,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
    },
    validateStatus: status => (status >= 200 && status < 300) || (status === 500 || status === 400)
})

export default instance
