import React from 'react'
import 'babel-polyfill'
import 'whatwg-fetch'
import { Provider } from 'react-redux'
import { render } from 'react-dom'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import { setupEnvironment } from 'helpers/'
import configureStore from 'helpers/store'
import Router from 'components/router'

import 'style/main.scss'

setupEnvironment()
const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)

function renderApp(AppRouter) {
    render(
        <Provider store={store}>
            <AppRouter history={history} />
        </Provider>,
        document.getElementById('root')
    )
}

renderApp(Router)

if (module.hot) {
    module.hot.accept('components/router', () => {
        const newRouter = require('components/router').default
        renderApp(newRouter)
    })
}
