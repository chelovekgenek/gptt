import { call, put, takeEvery } from 'redux-saga/effects'

import * as a from 'actions/users'
import * as c from 'constants/actions'
import * as api from './api/users'

function* handleResult(res, actionSuccess, actionError) {
    if (res.name === 'ValidationError') {
        yield put(actionError({
            name: res.message,
            fields: Object.keys(res.errors)
        }))
    } else {
        yield put(actionSuccess(res))
    }
}

function* handleGetRequest() {
    try {
        yield put(a.getSuccess(yield call(api.get())))
    } catch (error) {
        yield put(a.getError())
    }
}
function* handleCreateRequest({ payload }) {
    try {
        const result = yield call(api.create(payload))
        yield handleResult(result, a.createSuccess, a.createError)
    } catch (error) {
        yield put(a.createError())
    }
}
function* handleUpdateByIdRequest({ payload }) {
    try {
        const result = yield call(api.updateById(payload.id, payload.data))
        yield handleResult(result, a.updateByIdSuccess, a.updateByIdError)
    } catch (error) {
        yield put(a.updateByIdError())
    }
}
function* handleDeleteByIdRequest({ payload }) {
    try {
        const result = yield call(api.deleteById(payload))
        yield put(a.deleteByIdSuccess(result._id))
    } catch (error) {
        yield put(a.deleteByIdError())
    }
}

export default function* watcherHandleIPRequest() {
    yield takeEvery(c.API__USERS__GET__REQUEST, handleGetRequest)
    yield takeEvery(c.API__USERS__CREATE__REQUEST, handleCreateRequest)
    yield takeEvery(c.API__USERS__UPDATE_BY_ID__REQUEST, handleUpdateByIdRequest)
    yield takeEvery(c.API__USERS__DELETE_BY_ID__REQUEST, handleDeleteByIdRequest)
}
