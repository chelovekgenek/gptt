import axios from 'helpers/axios'

const BASE = 'users'

export const get = () => async () => (await axios.get(`${BASE}`)).data
export const create = data => async () => (await axios.post(`${BASE}`, data)).data
export const updateById = (id, data) => async () => (await axios.post(`${BASE}/${id}`, data)).data
export const deleteById = id => async () => (await axios.delete(`${BASE}/${id}`)).data
