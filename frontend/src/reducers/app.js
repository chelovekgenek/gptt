import * as c from 'constants/actions'

const initialState = {
    data: [],
    selected: {
        id: null,
        fetching: false
    },
    fetching: false,
    error: {
        name: '',
        fields: []
    }
}

export default function (state = initialState, action) {
    let index = null
    let data = null
    switch (action.type) {
        case c.API__USERS__GET__REQUEST:
            return { ...state, fetching: true }
        case c.API__USERS__GET__SUCCESS:
            return { ...state, fetching: false, data: action.payload }
        case c.API__USERS__GET__ERROR:
            return { ...state, fetching: false }

        case c.API__USERS__UPDATE_BY_ID__REQUEST: return {
            ...state,
            selected: {
                ...state.selected,
                id: action.payload.id,
                fetching: true
            },
            error: initialState.error
        }
        case c.API__USERS__UPDATE_BY_ID__SUCCESS:
            index = state.data.findIndex(item => item._id, action.payload._id)
            data = state.data
            if (index !== -1) data[index] = action.payload
            return {
                ...state,
                data,
                selected: initialState.selected
            }
        case c.API__USERS__DELETE_BY_ID__REQUEST: return {
            ...state,
            selected: {
                fetching: true,
                id: action.payload
            }
        }
        case c.API__USERS__DELETE_BY_ID__SUCCESS: return {
            ...state,
            data: state.data.filter(item => item._id !== action.payload),
            selected: initialState.selected
        }

        case c.API__USERS__CREATE__REQUEST: return {
            ...state,
            selected: {
                ...initialState.selected,
                fetching: true
            },
            error: initialState.error
        }
        case c.API__USERS__CREATE__SUCCESS: return {
            ...state,
            data: state.data.concat(action.payload),
            selected: initialState.selected
        }
        case c.API__USERS__CREATE__ERROR:
        case c.API__USERS__UPDATE_BY_ID__ERROR:
            return {
                ...state,
                selected: initialState.selected,
                error: action.payload
            }

        case c.USERS__CLEAR_ERRORS: return {
            ...state,
            error: initialState.error
        }

        default:
            return state
    }
}
