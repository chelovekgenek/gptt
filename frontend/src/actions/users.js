import * as a from 'constants/actions'

export const clearErrors = () => ({
    type: a.USERS__CLEAR_ERRORS
})

export const getRequest = () => ({
    type: a.API__USERS__GET__REQUEST
})
export const getSuccess = payload => ({
    type: a.API__USERS__GET__SUCCESS,
    payload
})
export const getError = () => ({
    type: a.API__USERS__GET__ERROR
})

export const createRequest = payload => ({
    type: a.API__USERS__CREATE__REQUEST,
    payload
})
export const createSuccess = payload => ({
    type: a.API__USERS__CREATE__SUCCESS,
    payload
})
export const createError = payload => ({
    type: a.API__USERS__CREATE__ERROR,
    payload
})

export const updateByIdRequest = (id, data) => ({
    type: a.API__USERS__UPDATE_BY_ID__REQUEST,
    payload: { id, data }
})
export const updateByIdSuccess = payload => ({
    type: a.API__USERS__UPDATE_BY_ID__SUCCESS,
    payload
})
export const updateByIdError = payload => ({
    type: a.API__USERS__UPDATE_BY_ID__ERROR,
    payload
})

export const deleteByIdRequest = id => ({
    type: a.API__USERS__DELETE_BY_ID__REQUEST,
    payload: id
})
export const deleteByIdSuccess = payload => ({
    type: a.API__USERS__DELETE_BY_ID__SUCCESS,
    payload
})
export const deleteByIdError = () => ({
    type: a.API__USERS__DELETE_BY_ID__ERROR
})
