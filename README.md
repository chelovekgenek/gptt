# Requirement installations
- [NodeJS](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/download-center#community)


# Backend
- ```cd ./backend```
- create storage folder: ```./storage/images```
## Run
- ```npm install```
- development: ```npm run dev```
- production: ```npm start```


# Frontend
- ```cd ./frontend```

## Run in development
- ```npm install```
- configure configuration file in project_dir/config/default.json
- ```npm start```

## Run in production
- set up ```NODE_ENV``` environment variable to ```production```
- ```npm install```
- ```npm run build```
- configuration file: project_dir/config/production.json
- ```npm start```
